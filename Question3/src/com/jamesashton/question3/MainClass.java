package com.jamesashton.question3;

import java.util.Vector;

public class MainClass {

	public static void main(String[] args) 
	{
		Vector<Payment> payments = new Vector<Payment>();
		payments.add(new Payment(100));
		payments.add(new Payment(50));

		System.out.println(recursive_sum(payments,0));
	}
	

	static int recursive_sum(Vector<Payment> payments, int startIndex)
	{
		return (startIndex < payments.size() ) ? recursive_sum(payments, startIndex + 1) + payments.get(startIndex).sum : 0;
	}
}
