package original;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestExpression {

	Expression expression;
	
	@Before
	public void setup()
	{
		expression = new Expression(new Expression(
                new Expression(5, "+", 4), "*", new Expression(6)), "/",
                new Expression(3, "-", 1));
	}

	
	@Test
	public void testEvaluate() 
	{
        int result = expression.Evaluate();
        
        assertTrue(result == 27);
	}
	
	@Test
	public void testPrettyPrint()
	{
		String result = expression.PrintString();
		
		assertTrue(result.equals("(((5 + 4) * 6) / (3 - 1))"));
		
	}
	
	@Test
	public void testSerializeToXml()
	{
		String path = "/Users/james/Development/Work/sts workspaces/Riplife/Question7/output/original.xml";
		
		boolean success = expression.SerializeToXml(path);
		
		assertTrue(success);  // for the purposes of this demo, XML output is assumed to be as expected at this point.
		
        
    }
	
	
	@Test 
	public void testExpressionChecking()
	{
		
		try
		{
			Expression invalidExpression = new Expression(new Expression(
                new Expression(5, "%", 4), "*", new Expression(6)), "/",
                new Expression(3, "-", 1));
		
			fail("Expressions containing a '%' operator are invalid.");
		}
		catch (IllegalArgumentException e)
		{
			// this exception is expected.
		}
		catch (Exception ex)
		{
			// other runtime exceptions might be thrown when an invalid expression is provided.
		}
	}

}
