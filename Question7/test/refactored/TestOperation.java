package refactored;

import static org.junit.Assert.*;

import org.junit.Test;

import static refactored.Operation.*;
public class TestOperation
{

	@Test
	public void testAdd()
	{
		assertTrue( calculate( add(1,2) ) == 3);
	}

	@Test
	public void testSubtract()
	{
		assertTrue( calculate( subtract(5,4) ) == 1);
	}

	@Test
	public void testMultiply()
	{
		assertTrue( calculate( multiply(3,4) ) == 12);
	}
	
	@Test
	public void testDivide()
	{
		assertTrue( calculate( divide(12,4) ) == 3 );
		try
		{
			assertTrue( calculate( divide(12,0) ) == 1 );
			fail("Arithmetic exception expected when dividing by 0.");
		}
		catch (ArithmeticException ex)
		{
			// expect this exception when dividing by 0
		}
	}
	@Test
	public void testExpressionAdd()
	{
		assertTrue( calculate( add(add(12,12),add(12,12)) ) == 48 );
	}
	@Test
	public void testExpressionSubtract()
	{
		assertTrue( calculate( subtract(add(12,12),add(6,6)) ) == 12 );
	}
	@Test
	public void testExpressionMultiply()
	{
		assertTrue( calculate( multiply(add(6,6),add(6,6)) ) == 144 );
	}
	@Test
	public void testExpressionDivide()
	{
		assertTrue( calculate( divide(add(12,12),add(6,6)) ) == 2 );
	}
	@Test
	public void testExpression()
	{
		int result = calculate(
						add( 
							divide(
										add(10,10), 
										divide(4,2)
								   ),
							multiply(
										subtract(90,45), 
										2
									)
						)
					);
		assertTrue(result == 100);
		
	}
	@Test
	public void testPrint()
	{
		Expression expression = divide ( multiply( add( 5, 4), 6), subtract(3,1) );
		
		assertTrue("The original example expression should still return 27.", expression.Evaluate() == 27);
		
		String result = print ( divide ( multiply( add( 5, 4), 6), subtract(3,1) ) );
		
		assertTrue("The output of pretty-printing must be the same after refactoring. Expected:  (((5 + 4) * 6) / (3 - 1))  ,  Actual:  " + result , result.equals("(((5 + 4) * 6) / (3 - 1))"));
	
	}
}


