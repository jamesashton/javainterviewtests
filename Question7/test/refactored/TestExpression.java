package refactored;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Before;
import org.junit.Test;

import refactored.Expression;

public class TestExpression {
	
	private static final String OUTPUT_LOCATION = "/Users/james/Development/Work/sts workspaces/Riplife/Question7/output/";
	private Expression expression;
	
	@Before
	public void setup()
	{
		expression = new Expression(new Expression(
                new Expression(5, "+", 4), "*", new Expression(6)), "/",
                new Expression(3, "-", 1));
	}
	@Test
	public void testEvaluate() 
	{
        int result = expression.Evaluate();
        
        assertTrue("The evaluation of the expression must return the same result after refactoring.", result == 27);
	}
	
	
	@Test
	public void testPrettyPrint()
	{
		String result = expression.PrintString();
		
		assertTrue("The output of pretty-printing must be the same after refactoring." , result.equals("(((5 + 4) * 6) / (3 - 1))"));
		
	}
	
	
	@Test
	public void testSerializeToXml()
	{
		boolean
		
		success = expression.SerializeToXml(getOutputPath("refactored.xml"));
		
		assertTrue(success); 
		
		// compare xml output of refactored code with xml output of the original code to ensure this does not change.
		assertTrue( "The output of XML serialization must not change after refactoring.", getXmlAsString(getOutputPath("original.xml")).equals(getXmlAsString(getOutputPath("refactored.xml"))) );
        
    }
	
	@Test 
	public void testExpressionChecking()
	{
		
		try
		{
			Expression invalidExpression = new Expression(new Expression(
                new Expression(5, "%", 4), "*", new Expression(6)), "/",
                new Expression(3, "-", 1));
		
			fail("Expressions containing a '%' operator are invalid.");
		}
		catch (IllegalArgumentException e)
		{
			// this exception is expected.
		}
		catch (Exception ex)
		{
			// other runtime exceptions might be thrown when an invalid expression is provided.
		}
	}
	
	private String getOutputPath(String filename)
	{
		return  OUTPUT_LOCATION + filename;
	}
	
	private String getXmlAsString(String filename)
	{
		StringBuilder result = new StringBuilder(); 
		try {
			FileInputStream fileInputStream;
		
			fileInputStream = new FileInputStream(filename);
	
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
			result = new StringBuilder();
			String line;
			while ((line = bufferedReader.readLine())!= null) 
			{  
				result.append(line);
			}
			bufferedReader.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}	
		return result.toString();
	}
}
