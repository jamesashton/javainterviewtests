package refactored;

import static refactored.Operator.ADD;
import static refactored.Operator.SUBTRACT;
import static refactored.Operator.MULTIPLY;
import static refactored.Operator.DIVIDE;

/*
 * The refactoring of Expression with the introduction of the Operator Enum provided the opportunity to model
 * operations as static imports which is a much more natural way to express arithmetic programmatically
 * than by the use of nested constructors with an operator argument. 
 */
public class Operation
{
	public static Expression add(int leftValue, int rightValue)
	{
		return ADD.operate(leftValue, rightValue);
	}
	public static Expression subtract(int leftValue, int rightValue)
	{
		return SUBTRACT.operate(leftValue, rightValue);
	}
	public static Expression multiply(int leftValue, int rightValue)
	{
		return MULTIPLY.operate(leftValue, rightValue);
	}
	public static Expression divide(int leftValue, int rightValue)
	{
		return DIVIDE.operate(leftValue, rightValue);
	}
	public static Expression add(Expression left, Expression right)
	{
		return ADD.operate(left, right); 
	}
	public static Expression subtract(Expression left, Expression right)
	{
		return SUBTRACT.operate(left, right);
	}
	public static Expression multiply(Expression left, Expression right)
	{
		return MULTIPLY.operate(left, right);
	}
	public static Expression divide(Expression left, Expression right)
	{
		return DIVIDE.operate(left, right);
	}
	public static Expression add(int left, Expression right)
	{
		return ADD.operate(left, right.Evaluate()); 
	}
	public static Expression subtract(int left, Expression right)
	{
		return SUBTRACT.operate(left, right.Evaluate());
	}
	public static Expression multiply(int left, Expression right)
	{
		return MULTIPLY.operate(left, right.Evaluate());
	}
	public static Expression divide(int left, Expression right)
	{
		return DIVIDE.operate(left, right.Evaluate());
	}
	public static Expression add(Expression left, int right)
	{
		return ADD.operate(left.Evaluate(), right); 
	}
	public static Expression subtract(Expression left, int right)
	{
		return SUBTRACT.operate(left.Evaluate(), right);
	}
	public static Expression multiply(Expression left, int right)
	{
		return MULTIPLY.operate(left.Evaluate(), right);
	}
	public static Expression divide(Expression left, int right)
	{
		return DIVIDE.operate(left.Evaluate(), right);
	}
	public static int calculate(Expression expression)
	{
		return expression.Evaluate();
	}
	public static String print(Expression expression)
	{
		return expression.PrintString();
	}
	public static void save(Expression expression, String filepath)
	{
		expression.SerializeToXml( filepath );
	}
}
