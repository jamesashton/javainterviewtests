package refactored;

import java.util.HashSet;
import java.util.Set;

public enum Operator
{	
	
	ADD("+", "Add") 
	{
		public Expression operate(int left, int right) 
		{ 
			return new Expression(left + right); 
		}
		
	},
	SUBTRACT("-", "Subtract")
	{
		public Expression operate(int left, int right) 
		{ 
			return new Expression(left - right); 
		}
	},
	MULTIPLY("*", "Multiply")
	{
		public Expression operate(int left, int right) 
		{
			return new Expression(left * right);
		}
	},
	DIVIDE("/", "Divide")
	{
		public Expression operate(int left, int right) 
		{ 
			if (right == 0) 
			{
                throw new ArithmeticException("Cannot divide by 0.");
            }
            return new Expression (left / right); 
		}
	};

	private String symbol;
	private String name;
	
	private Operator(String symbol,String name)
	{
		this.symbol = symbol;
		this.name = name;
	}
	
	public abstract Expression operate(int left, int right);
	
	public Expression operate(Expression left, Expression right)
	{
		return operate(left.Evaluate(), right.Evaluate());
	}
	
	public String getSymbol()
	{
		return symbol;
	}

	public String getName()
	{
		return name;
	}

	public static final Set<String> ALL_OPERATORS = new HashSet<String>();
	static 
	{
		for(int i=0;i<Operator.values().length;i++)
		{
			ALL_OPERATORS.add(Operator.values()[i].getSymbol());
		}
	}
	
	public static Operator fromSymbol(String symbol)
	{
		switch(symbol)
		{
			case "+": return ADD;
			case "-": return SUBTRACT;
			case "*": return MULTIPLY;
			case "/": return DIVIDE;
			default:
				throw new IllegalArgumentException();
		}
	}
	
	
}
