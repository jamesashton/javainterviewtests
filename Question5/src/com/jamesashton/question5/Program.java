package com.jamesashton.question5;

import java.util.HashSet;
import java.util.Set;

public class Program {

	public static void main(String[] args) {
		Animal lion1 = new Animal(1, "lion");
		Animal lion2 = new Animal(1, "lion");
		Set<Animal> animals = new HashSet<Animal>();
		animals.add(lion1);
		animals.add(lion2);
		System.out.println("Number of animals: " + animals.size());

		
		
	}

}
