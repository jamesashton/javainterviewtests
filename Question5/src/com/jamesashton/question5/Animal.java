package com.jamesashton.question5;

public class Animal {

	int animalId;
	String animalName;
	
	public Animal(int animalID, String animalName)
	{
		this.animalId = animalID;
		this.animalName = animalName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + animalId;
		return result;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		if (!(other instanceof Animal)) return false;
		Animal otherAnimal = (Animal)other;
		return otherAnimal.animalId == animalId;
	}
}
