package com.jamesashton.question6;

import test.IValueProvider;
import test.SomeClass;

public class Option<T> {



	T value;
	
	public Option()
	{
	}
	
	public Option(boolean b, T someClass) 
	{
		this.value = someClass;
	}

	public static <T> Option<T> createEmptyOption() 
	{
		return new Option<T>();
	}

	public boolean hasValue() 
	{
		return value != null;
	}

	public static <T> Option<T> fromValue(T someClass)
	{
		return new Option<T>(true, someClass);
	}
	
	public  T getValue()
	{
		if(value == null)
		{
			throw new IllegalAccessError("Unable to retrieve value");
		}
		return value;
	}

	public T valueOrElse(IValueProvider<T> valueProvider) 
	{
		T rtn = null;
		if(hasValue())
		{
			rtn = getValue();
		}
		else
		{
			rtn = valueProvider.getValue();
		}
				
		return rtn;
	}
	
	/* I auto generated these using Eclipse and they will do the job so I will not change them for this question. A
	 * real world implementation of this class would typically have some notion of the value class and be able to perform 
	 * an equality based on a value class property which acts as a meaningful identifier such as a database id or a UUID */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Option other = (Option) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}
