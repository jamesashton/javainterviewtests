package com.jamesashton.question1;

public class Question1 {

	public static void main(String[] args) 
	{
		
		A a = new A();
		a.foo();
		
		B b = new B();
		b.foo();
		
		a = b;
		a.foo();
	}

}
